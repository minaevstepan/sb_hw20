// Copyright Epic Games, Inc. All Rights Reserved.

#include "SB_HW20.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, SB_HW20, "SB_HW20" );
