// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "SB_HW20GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class SB_HW20_API ASB_HW20GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
